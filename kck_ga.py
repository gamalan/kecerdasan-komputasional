#!/usr/bin/python3
import sys
import random
import math


class Individual(object):
    """
    Individual

    Attributes
    ----------
    genotype        : list contain gen, list
    mutation_prob   : mutation probability by genotype, list
    fitness         : fitness value of individual, float/double/decimal,
    age             : age of individual, int

    Methods
    -------
    add_age     : add current individual age by one
    """
    genotype = list()
    mutation_prob = list()
    fitness = 0
    age = 0

    def __init__(self, data_set):
        self.genotype = data_set
        for i in range(len(self.genotype)):
            self.mutation_prob.append(random.random())
        self.age = 1

    def add_age(self):
        """
        Add age to individual by one
        """
        self.age += 1

    def get_highest_gen(self):
        highest = 0
        for i in range(len(self.genotype)):
            if self.genotype[i] > highest:
                highest = self.genotype[i]
        return highest

    def get_smallest_gen(self):
        smallest = self.get_highest_gen()
        for i in range(len(self.genotype)):
            if self.genotype[i] < smallest:
                smallest = self.genotype[i]
        return smallest

    def __cmp__(self, other):
        """
        Comparator
        """
        return cmp(self.fitness, other.fitness)


class GeneticAlgorithm(object):
    """
    GeneticAlgorithm
    Attributes
    ----------
    population          : population of Individual, list
    population_limit    : limit of population per iteration, int
    fitness_value       : target fitness value, int
    age_limit           : age limit of individual per iteration, int
    generation_limit    : iteration limit, int
    crossover_rate      : probability of crossover, float/double/decimal
    crossover_alpha     : alpha for crossover, float/double/decimal
    crossover_type      : crossover type(single,multiple,whole), str
    mutation_rate       : probability of mutation, float/double/decimal
    mutation_type       : mutation type(single,multiple), str,
    mutation_creep      : mutation creep(for non uniform), float
    crossover_count     : crossover that happen(counter), int
    mutation_count      : mutation that happen(counter), int
    solution_found      : status of solution, bool
    solution_index      : index of solution, int
    avg_fitness         : average fitness value of current population, float/double/decimal

    Methods
    -------
    __random_until_not_in       : Generate random key until not in list
    __random_until_more_than    : Generate random key until more than current key, but less than max value
    __fitness                   : Calculate individual fitness by sum, if individual fitness is equal or more than defined target fitness, set solution found to True
    __average_fitness           : Get average fitness of current population, and set it to avg_fitness Attributes
    __reject_old_age            : Kill/Remove all individual whose reaching the age limit
    __rank_population           : Rank current population, descending by its fitness value
    __breed                     : Breed two individual to create two child
    __crossover                 : Create new individual by crossover the parents gen
    __mutate                    : Create new individual by mutating the parent
    run                         : Run the GA
    """
    population = list()
    population_limit = 8
    fitness_value = 0
    age_limit = 0
    generation_limit = 10
    crossover_rate = 1
    mutation_rate = 0
    mutation_type = "single"
    mutation_creep = 0.25
    crossover_type = "single"
    crossover_alpha = 0.5
    solution_found = False
    solution_index = -1
    avg_fitness = 0
    crossover_count = 0
    mutation_count = 0

    def __init__(self, initial_population, fit_val=100, age_lmt=5, gen_limit=10, pop_limit=10, co_rate=0.8, co_type="single", co_alpha=0.5, mt_type="single", mt_creep=0.25):
        self.population = initial_population
        self.fitness_value = fit_val
        self.population_limit = pop_limit
        self.generation_limit = gen_limit
        self.age_limit = age_lmt
        self.crossover_rate = co_rate
        self.crossover_type = co_type
        self.crossover_alpha = co_alpha
        self.mutation_rate = 1 - self.crossover_rate
        self.mutation_type = mt_type
        self.mutation_creep = mt_creep

    def __random_until_not_in(self, key, key_temp, min_v, max_v):
        """
        Generate random key until not in list

        Parameters
        ----------
        key         : current key, int
        key_temp    : list of current key, list
        min_v       : min value of key, usually 0, int
        max_v       : max value of key, int

        Returns
        -------
        key         : key outside of current key list/key_temp, int
        """
        while key in key_temp:
            key = random.randint(min_v, max_v)
        return key

    def __random_until_more_than(self, key, max_v):
        """
        Generate random key until more than current key, but less than max value

        Parameters
        ----------
        key         : current key, int
        max_v       : max value of key, int

        Returns
        -------
        temp         : key more than current key, but less than max_v
        """
        temp = key - 1
        while temp <= key:
            temp = random.randint(key, max_v)
        return temp

    def __fitness(self, individual):
        """
        Calculate individual fitness by sum, if individual fitness is equal or more than defined target fitness, set solution found to True

        Parameters
        ----------
        individual  : Individual to be calculated its fitness, Individual

        Returns
        -------
        fitness     : individual value of fitness, int
        """
        individual.fitness = math.fsum(individual.genotype)
        # Check individual fitness if equal or more than target fitness
        if individual.fitness >= self.fitness_value:
            self.solution_found = True
            self.solution_index = self.population.index(individual)
        return individual.fitness

    def __average_fitness(self):
        """
        Get average fitness of current population, and set it to avg_fitness Attributes

        Parameters
        ----------

        Returns
        -------
        """
        fitness_total = 0
        for i in range(len(self.population)):
            fitness_total += self.__fitness(self.population[i])
        self.avg_fitness = fitness_total / len(self.population)
        return None

    def run(self):
        """
        Run the GA

        Parameters
        ----------

        Returns
        -------
        """
        iteration = 0
        # Calculate the average fitness first, to check if solution is exist in
        # initial population
        self.__average_fitness()
        # run until reach iteration limit or solution is found
        while iteration < self.generation_limit and self.solution_found != True:
            iteration += 1
            print ("Iteration : {0} Start".format(iteration))
            print ("Average Fitness : {0}".format(self.avg_fitness))
            key_temp = list()
            # Selection process, by age and rank
            self.__average_fitness()
            self.__reject_old_age()
            self.__sort_by_fitness(_reverse=True)
            self.__stochastic_universal_sampling()
            self.__average_fitness()
            key_count = len(self.population)
            pop_child = list()
            # Breeding process
            while key_count > 0:
                key1 = self.__random_until_not_in(random.randint(
                    0, self.population_limit - 1), key_temp, 0, self.population_limit - 1)
                #key1 = self.roulette_optimize(key_temp)
                key_temp.append(key1)
                key2 = self.__random_until_not_in(random.randint(
                    0, self.population_limit - 1), key_temp, 0, self.population_limit - 1)
                #key2 = self.roulette_optimize(key_temp)
                key_temp.append(key2)
                key_count -= 2
                print ("Breed {0} {1}".format(key1, key2))
                child1, child2 = self.__breed(
                    self.population[key1], self.population[key2])
                pop_child.append(child1)
                pop_child.append(child2)
                if key_count == 1:
                    break
            # Append child to current population
            self.population += pop_child[:]
            # Calculate fitness, search the solution
            self.__average_fitness()
            print ("Iteration : {0} Finish".format(iteration))
            print ("Average Fitness : {0}".format(self.avg_fitness))
        if self.solution_found:
            print ("Solution found, print best!\nBest Individual Gen : {0}\nIndex in population : {1}\nFitness Value : {2}".format(
                self.population[self.solution_index].genotype, self.solution_index, self.population[self.solution_index].fitness))
        else:
            self.__sort_by_fitness(_reverse=True)
            print ("Solution not found, print current best!\nBest Individual Gen : {0}\nFitness Value : {1}".format(
                self.population[0].genotype, self.population[0].fitness))
        print ("Crossover count : {0}\nMutation count : {1}".format(
            self.crossover_count, self.mutation_count))
        return None

    def __stochastic_universal_sampling(self):
        """
        Baker Stochastic Universal Sampling

        Parameters
        ----------

        Returns
        -------
        """
        total = 0
        for i in range(len(self.population)):
            total += self.population[i].fitness
        p = total / self.population_limit
        start = random.uniform(0, p)
        pointer = list()
        for i in range(len(self.population)):
            pointer.append(start + (i * p))
        self.__rws(pointer=pointer)

    def __rws(self, pointer):
        """
        Roulette wheel using baker SUS

        Parameters
        ----------

        Returns
        -------
        """
        keep = list()
        i = 0
        stop = False
        for j in range(len(pointer)):
            while self.__sum_fitness(i) < pointer[j] and i + 1 < len(pointer):
                i += 1
            print ("i"+str(i))
            keep.append(self.population[i])
            if len(keep) == self.population_limit:
                break
        self.population = keep[:]

    def __sum_fitness(self, end):
        sum_fitness = 0
        if end == 0:
            sum_fitness = self.population[0].fitness
        else:
            for i in range(0, end):
                sum_fitness += self.population[i].fitness
        return sum_fitness

    def __sort_by_fitness(self, _reverse=False):
        """
        Rank current population, descending by its fitness value

        Parameters
        ----------

        Returns
        -------
        """
        self.population.sort(
            key=lambda Individual: Individual.fitness, reverse=_reverse)

    def __reject_old_age(self):
        """
        Kill/Remove all individual whose reaching the age limit

        Parameters
        ----------

        Returns
        -------
        """
        # find index of individual that reaching old_age, store it to list
        for i in range(len(self.population)):
            if i < len(self.population) and self.population[i].age > self.age_limit:
                print ("Individual died of old age limit : {0}, with fitness : {1}".format(
                    self.population[i].genotype, self.population[i].fitness))
                del self.population[i]
                i -= 1
        return None

    def __breed(self, ind1, ind2):
        """
        Breed two individual to create two child

        Parameters
        ----------
        ind1        : First individual, Individual
        ind2        : Second individual, Individual

        Returns
        -------
        newInd1     : First child, Individual
        newInd2     : Second child, Individual
        """
        newInd1, newInd2 = Individual([]), Individual([])
        # generate random number, if it more than crossover rate, then do
        # mutation
        if (random.random() < self.crossover_rate):
            # increment crossover count
            self.crossover_count += 1
            # do crossover
            newInd1, newInd2 = self.__crossover(ind1, ind2)
        else:
            # increment mutation count
            self.mutation_count += 1
            # do crossover
            newInd1, newInd2 = self.__crossover(ind1, ind2)
            # do mutation
            newInd1 = self.__mutate(newInd1)
            newInd2 = self.__mutate(newInd2)
        # increment parent age
        ind1.add_age()
        ind2.add_age()
        return newInd1, newInd2

    def __mutate(self, individual):
        """
        Create new individual by mutating the parent
        Mutation style  :
        Creep increment
        Mutation type   :
        1. Uniform
        2. Non-Uniform

        Parameters
        ----------
        individual  : individual to be mutated/parent, Individual

        Returns
        -------
        newInd      : Child from the mutation, Individual
        """
        # copy parent gen
        data_temp = individual.genotype[:]
        key_temp = list()
        mutation_probs = random.random()
        print ("Mutation {0}, Mutation Prob : {1}".format(
            self.mutation_type, mutation_probs))
        print ("Before {0}".format(individual.genotype))
        key_mutated = list()
        for i in range(len(data_temp)):
            if individual.mutation_prob[i] < mutation_probs:
                key_mutated.append(i)
                if self.mutation_type == "uniform":
                    data_temp[i] = random.uniform(
                        individual.get_smallest_gen(), individual.get_highest_gen())
                else:
                    data_temp[i] += self.mutation_creep
        print ("Mutated in {0}".format(key_mutated))
        print ("After {0}".format(data_temp))
        return Individual(data_temp)

    def __crossover(self, ind1, ind2):
        """
        Create new individual by crossover the parents gen
        Crossover style  :
        Arithmetic
        Crossover type   :
        1. Single
        2. Multiple
        3. Whole

        Parameters
        ----------
        ind1        : First individual, Individual
        ind2        : Second individual, Individual

        Returns
        -------
        newInd1     : First child, Individual
        newInd2     : Second child, Individual
        """
        # clone parent gen to temporal list
        data_temp, data_temp2 = ind1.genotype[:], ind2.genotype[:]
        if self.crossover_type == "single":
            crossover_key = random.randint(0, len(ind1.genotype) - 1)
            crossover_key2 = crossover_key
        elif self.crossover_type == "multiple":
            crossover_key = random.randint(0, len(ind1.genotype) - 3)
            crossover_key2 = self.__random_until_more_than(
                crossover_key, len(ind1.genotype) - 1)
        else:
            crossover_key, crossover_key2 = 0, len(ind1.genotype) - 1
        print ("Crossover {0} {1} {2}".format(
            self.crossover_type, crossover_key, crossover_key2))
        print ("Before ")
        print (data_temp)
        print (data_temp2)
        for i in range(crossover_key, crossover_key2 + 1):
            data_temp[i] = (self.crossover_alpha * ind1.genotype[i]) + \
                ((1 - self.crossover_alpha) * ind2.genotype[i])
            data_temp2[i] = (self.crossover_alpha * ind2.genotype[i]) + \
                ((1 - self.crossover_alpha) * ind1.genotype[i])
        print ("After ")
        print (data_temp)
        print (data_temp2)
        return Individual(data_temp), Individual(data_temp2)

if __name__ == '__main__':
    """
    Generate/randomize initial population
    """
    genotype_size = 10  # genotype size
    initial_population_size = 100  # initial population size
    max_gen_val = 50  # max gen value in genotype
    individidual_ = list()
    # randomize
    for i in range(initial_population_size):
        temp_gen = list()
        for j in range(genotype_size):
            temp_gen.append(random.random() * random.randint(0, max_gen_val))
        individidual_.append(Individual(temp_gen))
    print (len(individidual_))
    for i in range(initial_population_size):
        print ("{0} {1}".format(i, individidual_[i].genotype))
    # Create GA instance
    GA = GeneticAlgorithm(individidual_, co_type="multiple", gen_limit=200, pop_limit=50,
                          co_alpha=0.75, co_rate=0.90, mt_type="non_uniform", fit_val=500, age_lmt=5, mt_creep=3.5)
    # Run GA instance
    GA.run()
